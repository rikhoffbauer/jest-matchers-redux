# @jest-matchers/redux
![coverage](https://gitlab.com/rikhoffbauer/jest-matchers-redux/badges/master/coverage.svg)
![build](https://gitlab.com/rikhoffbauer/jest-matchers-redux/badges/master/build.svg)
![npm version](https://badge.fury.io/js/%40jest-matchers%2Fredux.svg)

Jest matchers for redux.

## Installing

##### Using npm

`npm install @jest-matchers/redux`

##### Using yarn

`yarn add @jest-matchers/redux`

## Getting started
To get started with @jest-matchers/redux you need to import it before your tests are ran.
This can be done in a `setupTestFrameworkScriptFile` or `setupFiles` file.

#####  `setupTests.js` 
```typescript
require("@jest-matchers/redux");
```

## Usage

### expect(mockStore).toHaveDispatched(action);
This function can be used to test whether an action has been dispatched by a (mock) store. 
```typescript
expect(store).toHaveDispatched("SOME_ACTION");
expect(store).toHaveDispatched({type: "SOME_ACTION"});
```
         
### expect(action).toHaveBeenDispatchedBy(mockStore)
This function can be used to test whether an action has been dispatched by a (mock) store.
```typescript
expect("SOME_ACTION").toHaveBeenDispatchedBy(store);
expect({type: "SOME_ACTION"}).toHaveBeenDispatchedBy(store);
```

### expect(mockStore).toReduceActionToState(action, state)
This function can be used to test if an action is reduced correctly. 
 ```typescript
expect(mockStore).toReduceActionToState("SOME_ACTION", "SOME_STATE");
expect(mockStore).toReduceActionToState({type: "SOME_ACTION"}, "SOME_STATE");
```

## Contributing
Contributions in the form of pull requests and bug reports are appreciated.

### Running tests

Tests are ran using the `test` npm script.

##### Using npm

`npm test`

##### Using yarn

`yarn test`

import extendExpect from "../src/extendExpect";
import toHaveBeenDispatchedBy from "../src/matchers/toHaveBeenDispatchedBy";
import toHaveDispatched from "../src/matchers/toHaveDispatched";
import toReduceActionToState from "../src/matchers/toReduceActionToState";

describe("extendExpect", () => {
    const shouldExtendExpectWith = (name: string, fn: Function) => {
        const extend = jest.fn();
        const xpct = (global as any).expect;
        (global as any).expect = { extend };
        extendExpect();
        (global as any).expect = xpct;
        expect(extend).toHaveBeenCalledWith(
            expect.objectContaining({
                [name]: fn,
            }),
        );
    };

    it("should extend describe with the toHaveDispatched method", () => {
        shouldExtendExpectWith(`toHaveDispatched`, toHaveDispatched);
    });

    it("should extend describe with the toReduceActionToState method", () => {
        shouldExtendExpectWith(`toReduceActionToState`, toReduceActionToState);
    });

    it("should extend describe with the toHaveBeenDispatchedBy method", () => {
        shouldExtendExpectWith(`toHaveBeenDispatchedBy`, toHaveBeenDispatchedBy);
    });
});

describe("index", () => {
    it("should extend expect when imported", () => {
        const extendExpect = require("../src/extendExpect").default;
        const spy = jest.fn();
        require("../src/extendExpect").default = spy;
        require("../src");

        expect(spy).toHaveBeenCalled();

        require("../src/extendExpect").default = extendExpect;
    });
});

import { createMockStore } from "redux-test-utils";
import toHaveBeenDispatchedBy from "../../src/matchers/toHaveBeenDispatchedBy";

describe("expect(action).toHaveBeenDispatchedBy(store)", () => {
    expect.extend({
        toHaveBeenDispatchedBy,
    });

    it("should work with a string as action", () => {
        const store = createMockStore();
        const action = "TEST_ACTION";

        expect(action).not.toHaveBeenDispatchedBy(store);

        store.dispatch({ type: action });

        expect(action).toHaveBeenDispatchedBy(store);
    });

    it("should work with an object as action", () => {
        const store = createMockStore();
        const action = { type: "TEST_ACTION" };

        expect(action).not.toHaveBeenDispatchedBy(store);
        expect(() => expect(action).toHaveBeenDispatchedBy(store)).toThrow();

        store.dispatch(action);

        expect(action).toHaveBeenDispatchedBy(store);
        expect(() => expect(action).not.toHaveBeenDispatchedBy(store)).toThrow();
    });
});

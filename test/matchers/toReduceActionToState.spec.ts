import { createStore } from "redux";
import toReduceActionToState from "../../src/matchers/toReduceActionToState";

describe("expect(store).toReduceActionToState(action)", () => {
    expect.extend({
        toReduceActionToState,
    });

    const createReducer = (actionType: string) => (result: any) => (
        state = "",
        _action: any,
    ) => (_action.type === actionType ? result : state);

    it("should work with a string as action", () => {
        const action = "TEST_ACTION";
        const result = "RESULT";
        const reducer = createReducer(action)(result);

        expect(reducer).toReduceActionToState(action, result);
        expect(reducer).not.toReduceActionToState(action, result + "b");

        expect(() =>
            expect(reducer).not.toReduceActionToState(action, result),
        ).toThrow();
        expect(() =>
            expect(reducer).toReduceActionToState(action, result + "b"),
        ).toThrow();
    });

    it("should work with an object as action", () => {
        const action = { type: "TEST_ACTION" };
        const result = "RESULT";
        const reducer = createReducer(action.type)(result);

        expect(reducer).toReduceActionToState(action, result);
        expect(reducer).not.toReduceActionToState(action, result + "b");

        expect(() =>
            expect(reducer).not.toReduceActionToState(action, result),
        ).toThrow();
        expect(() =>
            expect(reducer).toReduceActionToState(action, result + "b"),
        ).toThrow();
    });

    it("should work with a store", () => {
        const action = { type: "TEST_ACTION" };
        const result = "RESULT";
        const reducer = createReducer(action.type)(result);
        const store = createStore(reducer);

        expect(store).toReduceActionToState(action, result);
        expect(store).not.toReduceActionToState(action, result + "b");

        expect(() =>
            expect(store).not.toReduceActionToState(action, result),
        ).toThrow();
        expect(() =>
            expect(store).toReduceActionToState(action, result + "b"),
        ).toThrow();
    });
});

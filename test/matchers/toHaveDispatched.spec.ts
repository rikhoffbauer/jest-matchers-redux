import { createMockStore } from "redux-test-utils";
import toHaveDispatched from "../../src/matchers/toHaveDispatched";

describe("expect(store).toHaveDispatched(action)", () => {
    expect.extend({
        toHaveDispatched,
    });

    it("should work with a string as action", () => {
        const store = createMockStore();
        const action = "TEST_ACTION";

        expect(store).not.toHaveDispatched(action);

        store.dispatch({ type: action });

        expect(store).toHaveDispatched(action);
    });

    it("should work with an object as action", () => {
        const store = createMockStore();
        const action = { type: "TEST_ACTION" };

        expect(store).not.toHaveDispatched(action);
        expect(() => expect(store).toHaveDispatched(action)).toThrow();

        store.dispatch(action);

        expect(store).toHaveDispatched(action);
        expect(() => expect(store).not.toHaveDispatched(action)).toThrow();
    });
});

import { join } from "path";
import { Configuration } from "webpack";

const WebpackShellPlugin = require("webpack-shell-plugin");

const getPath = (...segments: string[]) => join(__dirname, ...segments);

const paths = {
    entry: getPath("src/index.ts"),
    outDir: getPath("lib"),
    outFile: "index.js",
};

const config: Configuration = {
    entry: paths.entry,
    devtool: "source-map",
    mode: "production",
    target: "node",
    output: {
        path: paths.outDir,
        libraryTarget: "commonjs2",
        filename: paths.outFile,
    },
    resolve: {
        extensions: [".ts", ".js"],
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: "awesome-typescript-loader",
            },
        ],
    },
    plugins: [
        new WebpackShellPlugin({
            safe: true,
            onBuildEnd: ["node_modules/.bin/tsc"],
        }),
    ],
};

export default config;

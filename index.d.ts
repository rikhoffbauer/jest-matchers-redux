import { mockStore } from "redux-test-utils";

declare global {
    namespace jest {

        interface Matchers<R> {

            /**
             * Used to test whether a (mock) store reduces an action correctly.
             * @example **Testing whether a (mock) store reduces an action to a given state**
             ```typescript
             expect(store).toReduceActionToState("SOME_ACTION", "SOME_STATE");
             expect(store).toReduceActionToState({type: "SOME_ACTION"}, "SOME_STATE");
             ```
             */
            toReduceActionToState<T>(action: {}, state: T): R;
            toReduceActionToState<T>(action: string, state: T): R;

            /**
             * Used to test whether a (mock) store has dispatched a given action.
             * @returns {R}
             * @example **Testing whether a (mock) store reduces an action to a given state**
             ```typescript
             expect(store).toHaveDispatched("SOME_ACTION");
             expect(store).toHaveDispatched({type: "SOME_ACTION"});
             ```
             */
            toHaveDispatched(action: string): R;
            toHaveDispatched(action: {}): R;

            /**
             * Used to test whether an action has been dispatched by a (mock) store.
             * @param {mockStore<S>} store
             * @returns {R}
             ```typescript
             expect("SOME_ACTION").toHaveBeenDispatchedBy(store);
             expect({type: "SOME_ACTION"}).toHaveBeenDispatchedBy(store);
             ```
             */
            toHaveBeenDispatchedBy<S>(store: mockStore<S>): R;
        }

    }
}

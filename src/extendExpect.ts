import toHaveBeenDispatchedBy from "./matchers/toHaveBeenDispatchedBy";
import toHaveDispatched from "./matchers/toHaveDispatched";
import toReduceActionToState from "./matchers/toReduceActionToState";

const extendExpect = () => {
    expect.extend({
        toHaveDispatched,
        toHaveBeenDispatchedBy,
        toReduceActionToState,
    });
};

export default extendExpect;

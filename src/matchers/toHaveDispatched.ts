import { mockStore } from "redux-test-utils";

export default function toHaveDispatched<State>(
    store: mockStore<State>,
    action: any,
) {
    const pass =
        typeof action === "string"
            ? store.isActionTypeDispatched(action)
            : store.isActionDispatched(action);

    const message = () =>
        `Expected store${this.isNot ? " not" : ""} to have dispatched:\n` +
        `  ${this.utils.printExpected(action)}`;

    return { message, pass };
}

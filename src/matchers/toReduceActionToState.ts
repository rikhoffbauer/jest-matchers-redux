import diff = require("jest-diff");
import { Reducer } from "@ts-commons/fp/types";
import { mockStore } from "redux-test-utils";

export default function toReduceActionToState<State>(
    reducerOrStore: Reducer<State, any> | mockStore<State>,
    action: any,
    state: State,
) {
    const _action = typeof action === "object" ? action : { type: action };
    let result: any;

    if (typeof reducerOrStore === "function") {
        result = reducerOrStore(undefined as any, _action);
    } else if (typeof reducerOrStore === "object") {
        reducerOrStore.dispatch(_action);
        result = reducerOrStore.getState();
    }

    const pass = this.equals(result, state);
    const diffString = diff(state, result, {
        expand: this.expand,
    });

    const message = () =>
        `Expected reducer${this.isNot ? " not" : ""} to reduce action:\n` +
        `  ${this.utils.printExpected(_action)}\n` +
        `To:\n` +
        `  ${this.utils.printExpected(state)}` +
        (!this.isNot
            ? `\nReceived:\n  ${this.utils.printReceived(result)}`
            : "") +
        (!this.isNot && diffString ? `\n\nDifference:\n\n${diffString}` : "");

    return { message, pass };
}

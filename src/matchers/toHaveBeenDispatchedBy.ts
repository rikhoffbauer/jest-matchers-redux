import { mockStore } from "redux-test-utils";
import toHaveDispatched from "./toHaveDispatched";

export default function toHaveBeenDispatchedBy<State>(
    action: any,
    store: mockStore<State>,
) {
    return toHaveDispatched.call(this, store, action);
}
